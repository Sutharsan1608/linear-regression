''' Date: 26/06/2018
    Function : Plot predicted values and data set same graph'''


def linearPredicter(location):   #locaation of csv file with dataset
    import pandas as pa
    import matplotlib.pyplot as plt


    df=pa.read_csv(location,)
    xaxis=df.iloc[:,1]
    yaxis=df.iloc[:,2]
    length=len(xaxis)
    x=0
    y=0
    xx=0
    yy=0
    xy=0
    xlist=[]
    for i in range(0,length-1):         #Cummulative summ of X
        xlist.append(xaxis.iloc[i])

        x=x+xaxis.iloc[i]
       
        
    for k in range(0,length-1):         #Cummulative summ of X^2
        xx=xx+xaxis.iloc[k]*xaxis.iloc[k]
        
    for l in range(0,length-1):          #Cummulative summ of Y  
        y=y+yaxis.iloc[l]

    for m in range(0,length-1):          #Cummulative summ of Y^2
        yy=yy+yaxis.iloc[m]*yaxis.iloc[m]

    for n in range(0,length-1):         #Cummulative summ of XY
        xy=xy+xaxis.iloc[n]*yaxis.iloc[n]
        
    beta=(length*xy-x*y)/(length*xx-x*x)    #Beta=Sxy/Sxx

    alpha=y/length-beta*x/length        #alpha=Ymean-Xmean*Beta

    ypredict=[]
    for cal in range(0,length-1):       
        ypredict.append(beta*xlist[cal]+alpha)  #list of predicted value
    plt.plot(xaxis,yaxis,linestyle='none', marker='o',label='Datas')
    
    plt.plot(xlist,ypredict,color='c',linewidth=4,label='Predicted line')
    
    plt.autoscale(tight=True)
    plt.legend()
    plt.show()



    """plt.bar(date,ypredict)
    plt.bar(date,ypredict, width = 0.0000000000000000000000001, color = 'red' , align = 'center')
    plt.autoscale(tight = True)
    plt.show()"""



  
linearPredicter('/Users/sutharsan/Desktop/Linear_Regression/output-soapSalesDataset.csv')
    